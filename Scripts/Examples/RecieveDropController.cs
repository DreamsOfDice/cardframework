﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardFrameworkCore;
using CardFrameworkUI;
using CardFrameworkExamples;

public class RecieveDropController : IDropIntoAreaInterface
{
    public void OnDropHandler(GameObject objectDropped, GameObject areaBeingDroppedOn)
    {
        CardUI cardPlayed = objectDropped.GetComponent<CardUI>();
        PlayAreaUI areaDroppedOn = areaBeingDroppedOn.GetComponent<PlayAreaUI>();

        Debug.Log("something was succesfully dropped");
        if (cardPlayed == null || areaDroppedOn == null) {
            return;
        }

        if (cardPlayed.GetCard()?.IsUseable() == true && areaDroppedOn.GetPlayAreaData().GetCardsInPile().IsCardPileFull()==false) {
            areaDroppedOn.AddCardToPlayArea(cardPlayed);
            /*objectDropped.transform.position = areaBeingDroppedOn.transform.position;
            objectDropped.transform.SetParent(areaBeingDroppedOn.transform);*/
        }
               
        
    }
}
