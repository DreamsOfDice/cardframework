﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using CardFrameworkCore;
using CardFrameworkUI;

namespace CardFrameworkTests
{
    public class CardsTest
    {
        Card cardExample;
        CardTemplate templateExample;
        GameObject cardObject;
        CardUI cardUIHandler;

        [SetUp]
        public void Setup() {
            templateExample = Resources.Load<CardTemplate>("TestResources/CardExample");
            cardObject = Resources.Load<GameObject>("TestResources/CardObjectBase");
            cardExample = new Card(templateExample);
            cardUIHandler = cardObject.GetComponent<CardUI>();

            cardUIHandler.SetCardData(cardExample);

        }

        // A Test behaves as an ordinary method
        [Test]
        public void CardGenerationTest()
        {
            Assert.AreEqual(cardExample.GetCardName(),templateExample.cardName);
            Assert.AreEqual(cardExample.GetDescription(), templateExample.Description);
            Assert.AreEqual(cardExample.GetDescription(), cardUIHandler.cardDescriptionText.text);
        }

        [Test]
        public void CardIsPlayableTest()
        {
            Assert.IsTrue(cardExample.IsUseable());
        }

        [Test]
        public void CardDetailsHaveBeenRequestedTest()
        {

        }

        [Test]
        public void CardHasBeenPlayedTest()
        {

        }

    }
}
