﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;
using CardFrameworkCore;
using CardFrameworkUI;


namespace CardFrameworkTests
{
    public class CardPileTests 
    {
        CardPile cardsTest;
        CardTemplate templateExample;
        CardTemplate templateExample2;
        Card card1;
        Card card2;

        [SetUp]
        public void Setup()
        {
            cardsTest = new CardPile(5);
            templateExample = Resources.Load<CardTemplate>("TestResources/CardExample");
            templateExample2 = Resources.Load<CardTemplate>("TestResources/CardExampleSlash");
            card1 = new Card(templateExample);
            card2 = new Card(templateExample2);

            cardsTest.AddCardToPile(card1);
            cardsTest.AddCardToPile(card2);
        }

        [Test]
        public void SetCardsIntoPileTest() {

          Assert.IsTrue(cardsTest.AddCardToPile(new Card(templateExample)));
        }

        [Test]
        public void GetSpecificCardTest()
        {
           
            Assert.AreEqual(cardsTest.GetCard(templateExample.cardName).GetCardName(), new Card(templateExample).GetCardName());
            Assert.AreEqual(cardsTest.GetCard(templateExample2.cardName).GetCardName(), new Card(templateExample2).GetCardName());
            
        }

        [Test]
        public void TryToGetNonExistentCard()
        {
            Assert.IsNull(cardsTest.GetCard(8));
            Assert.IsNull(cardsTest.GetCard("not a real name"));
        }

        [Test]
        public void GetMultipleCardsOfACertainType()
        {
            cardsTest.AddCardToPile(new Card(templateExample));

            Assert.AreEqual(2,cardsTest.GetAllCardsOfName(templateExample.cardName).Count);
            Assert.AreEqual(0, cardsTest.GetAllCardsOfName("Darth Nonexistant").Count);
        }

        [Test]
        public void RemoveCardFromPile()
        {
            Assert.IsTrue(cardsTest.RemoveCardFromPile(card1));
            Assert.AreEqual(0, cardsTest.GetAllCardsOfName(templateExample.cardName).Count);

        }

        [Test]
        public void RemoveNonExistentCardFromPile()
        {

            Assert.IsFalse(cardsTest.RemoveCardFromPile(88));
        }


    }
}
