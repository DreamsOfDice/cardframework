﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CardFrameworkCore
{
    public interface IClickHandler
    {
        Card OnClickInteraction(GameObject objectClicked);
        
}

}

