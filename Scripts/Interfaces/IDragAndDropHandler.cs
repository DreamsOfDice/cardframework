﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardFrameworkCore
{
    public interface IDragAndDropHandler
    {
        void OnDragBeginHandler(GameObject objectBeingDragged);
        void OnDragHandler(GameObject objectBeingDragged, Vector3 positionBeingDraggedTo);
        void OnEndDragHandler(GameObject objectBeingDragged, Vector3 resetPosition, bool shouldReset);
    }

}
