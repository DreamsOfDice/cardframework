﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardFrameworkCore
{
    public interface IDropIntoAreaInterface
    {
        void OnDropHandler(GameObject objectDropped, GameObject areaBeingDroppedOn);

    }
}
