﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardFrameworkCore
{
    public class Card
    {
        string cardName;
        string cardDescription;
        CardTemplate templateRef;

        public Card(CardTemplate template)
        {
            cardName = template.cardName;
            cardDescription = template.Description;
            templateRef = template;
        }

        public virtual string GetCardName() {

            return cardName;
        }

        public virtual string GetDescription()
        {

            return cardDescription;
        }

        public virtual CardTemplate GetTemplate() {
            return templateRef;
        }

        public virtual bool IsUseable(){
            return true;

        }

        public virtual void UseCard() {

            Debug.Log("Card has been used");
        }
    }
}

