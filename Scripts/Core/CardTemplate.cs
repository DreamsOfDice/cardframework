﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardFrameworkCore
{
    [CreateAssetMenu(fileName = "NewBaseCardTemplate",menuName = "CardFramework Basic/CardTemplate")]
    public class CardTemplate : ScriptableObject
    {
        public string cardName;
        public string Description;
        public Sprite baseCardImage;
        public Sprite cardImage;


    }


}