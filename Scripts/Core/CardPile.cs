﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CardFrameworkCore
{
    public class CardPile 
    {
        protected List<Card> cards;
        protected int sizeOfPile;


        public CardPile(int size, List<Card>newCards = null) {

            sizeOfPile = size;

            if (newCards != null)
            {
                if (newCards.Count > sizeOfPile) {
                    cards = new List<Card>();
                    return;
                }
                cards = new List<Card>(newCards);

            }
            else {

                cards = new List<Card>();
            }
        }

        public virtual bool AddCardToPile(Card newCard) {

            if (cards.Count < sizeOfPile) {
                                 
                cards.Add(newCard); 
                return true;
            }
            return false;
        }

        public virtual bool RemoveCardFromPile(Card newCard)
        {
                      
            int temp = cards.FindIndex(filledSlot => filledSlot == newCard);
            if (temp < 0) {
                    return false;
            }
            cards.RemoveAt(temp);
           return true;
          
        }

        public virtual bool RemoveCardFromPile(int index) {

            if (cards.Count > index) {
                cards.RemoveAt(index);
                return true;
            } else {
                
             return false;
            }

        }

        public virtual Card GetCard(int index) {
            if (cards.Count > index)
            {
                return cards[index];

            }
            else {

                return null;
            }
        }

        public List<Card> GetAllCardsOfName(string cardName)
        {
            List<Card> cardsToReturn = new List<Card>();
            foreach (Card c in cards)
            {
                if (c.GetCardName() == cardName)
                {
                    cardsToReturn.Add(c);

                }

            }
            return cardsToReturn;
        }

        public virtual Card GetCard(string cardNameToFind) {
            foreach (Card c in cards) {
                if (c.GetCardName() == cardNameToFind){
                    return c;

                }
                
            }
            return null;
        }

        public virtual int GetPileTotalSize() {

            return sizeOfPile;
        }

        public virtual int GetAmountOfCardsInPile() {

            return cards.Count;

        }

        public virtual bool IsCardPileFull()
        {
            if (cards.Count < sizeOfPile) {
                return false;
            } else {
                return true;
            }
        }
    }
}
