﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardFrameworkCore
{
    public class PlayArea
    {
        CardPile cardsInPlay;
        int amountOfElementsPlayable;
        bool active = true;

        public PlayArea(int newAmount)
        {
            amountOfElementsPlayable = newAmount;
            cardsInPlay = new CardPile(amountOfElementsPlayable);
        }

        public virtual void SetStatus(bool status)
        {
            active = status;

        }

        public bool GetActiveStatus() {
            return active;
        }

        public CardPile GetCardsInPile() {
            return cardsInPlay;
        }
        
        public virtual int GetAmountIfElementsPlayable() {
            return amountOfElementsPlayable;
        }

     
        public virtual bool AddCardToPlayArea(Card newCardToAdd) {

            if (cardsInPlay.GetPileTotalSize() > cardsInPlay.GetAmountOfCardsInPile() && active)
            {
                cardsInPlay.AddCardToPile(newCardToAdd);
                return true;
            }
            else {
                return false;
            }


        }

    }

}
