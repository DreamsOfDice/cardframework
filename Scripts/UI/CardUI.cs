﻿using CardFrameworkCore;
using CardFrameworkExamples;
using CardFrameworkUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CardFrameworkUI { 

    public class CardUI : MonoBehaviour, IPointerClickHandler,IDragHandler,IEndDragHandler,IBeginDragHandler
    {
        #region delegates
        public delegate void CardReleasedEvent(CardUI obj);
        public CardReleasedEvent cardReleasedEventHandler;

        public delegate void CardDraggedEvent();
        public CardDraggedEvent CardDraggedEventHandler;

        public delegate void CardClickedEvent();
        public CardClickedEvent cardClickedEventHandler;
        #endregion

        #region variables
        public Text nameText;
        public Text cardDescriptionText;
        public Image cardBaseImage;
        public Image cardSprite;
              

        protected Card cardData;
        protected IDragAndDropHandler dragControl;
        protected IClickHandler clickControl;
        protected Vector3 positionOnBeginDrag;

        #endregion

        void Start() {
        /*    dragControl = new DragController();
            clickControl = new ClickController();
            Card testCard = new Card(Resources.Load<CardTemplate>("TestResources/CardExample"));
            SetCardData(testCard, dragControl,clickControl);*/
        }

        public virtual void SetCardData(Card c, IDragAndDropHandler newDragControler=null, IClickHandler newClickControler=null)
        {

            cardData = c;
            dragControl = newDragControler;
            clickControl = newClickControler;

            if(nameText!= null) nameText.text = cardData.GetTemplate().cardName;
            if (cardDescriptionText != null) cardDescriptionText.text = cardData.GetTemplate().Description;
            cardBaseImage.sprite = cardData.GetTemplate().baseCardImage;
            cardSprite.sprite = cardData.GetTemplate().cardImage;

            //cardReleasedEventHandler += cardData.UseCard;

        }


        #region Graphical Interaction Methods
        public void OnDrag(PointerEventData eventData)
        {
            if (dragControl == null) {
                return;
            }
            dragControl.OnDragHandler(gameObject,Input.mousePosition);
        }

        public void OnEndDrag(PointerEventData eventData)
        {        
            if (dragControl == null)
            {
                return;
            }

            dragControl.OnEndDragHandler(gameObject, positionOnBeginDrag, true);
            
            cardReleasedEventHandler?.Invoke(this);

        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (cardClickedEventHandler == null)
            {
                return;
            }
            Debug.Log("Clicked");
            cardClickedEventHandler?.Invoke();
        }
        
      
        public void OnBeginDrag(PointerEventData eventData)
        {
            positionOnBeginDrag = transform.localPosition;
            dragControl?.OnDragBeginHandler(gameObject);
            CardDraggedEventHandler?.Invoke();


        }
        #endregion

        #region Helper functions

        #endregion

        #region getters
        public Card GetCard() {

            return cardData;
        }
        #endregion
    }

}
